# ARCHIVED

This project has been replaced by [goAccess](https://gitlab.com/bibliosansfrontieres/olip/olip-deploy/-/compare/master...feature%2Fenvoy_support)

# Cockpit Starter Kit

Scaffolding for a [Cockpit](http://www.cockpit-project.org) module.

# Integration with python metrics software

- Root directory of this git repository has a statefile_config.json in which statefile_path must be defined. (default: /data/parser/statefile.json)
- This statefile is the output of the python parser.
- The python parser must be installed.

# Compiled version

Compiled version of the plugin is available at : https://bibliosansfrontieres.gitlab.io/olip/cockpit-plugin-metrics/plugin.tgz