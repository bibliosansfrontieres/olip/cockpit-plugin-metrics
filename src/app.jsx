import React from 'react';
import cockpit from 'cockpit';
import './app.scss';
import { Doughnut, Bar } from 'react-chartjs-2';
import { Tabs } from "@yazanaabed/react-tabs";

const EMPTY_GRAPH = {
    datasets: [{
        data: [],
        backgroundColor: [
        ]
    }],
    labels: []
}

export class Application extends React.Component {
    constructor() {
        super();
        this.getReqStatusCountData = this.getReqStatusCountData.bind(this);
        this.watchStatefile = this.watchStatefile.bind(this);
        this.watchArchivefile = this.watchArchivefile.bind(this);
        this.getSelectedDay = this.getSelectedDay.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.getReqArchiveCountData = this.getReqArchiveCountData.bind(this);

        this.state = {
            dailyLogs: [],
            date: null,
            selectedDay: {
                date: null,
                nbFailedRequests: null,
                nbSuccessRequests: null,
                accessedRoutes: null,
                remote_addresses: null
            },

            STATEFILE_PATH: process.env.STATEFILE_PATH,
            ARCHIVEFILE_PATH: process.env.ARCHIVEFILE_PATH,
            Categories: {
                name: null
            },
            selectedPeriod: {
                nbFailedRequests: null,
                nbSuccessRequests: null,
                accessedRoutes: null,
                remote_addresses: null
            }

        };
    }

    componentDidMount() {
        this.watchStatefile(); // WATCH STATEFILE (MODIFIED REGULARLY)
        this.watchArchivefile(); // WATCH ARCHIVE FILE (MODIFIED ONCE A DAY AT MOST)
    }

    watchStatefile() {
        cockpit.file(this.state.STATEFILE_PATH).watch((content) => {
            if (content) {
                var statefile = JSON.parse(content)
                const selectedDay = {
                    date: statefile.lastDailyLog.date,
                    nbSuccessRequests: statefile.lastDailyLog.nbSuccessRequests,
                    nbFailedRequests: statefile.lastDailyLog.nbFailedRequests,
                    accessedRoutes: statefile.lastDailyLog.accessedRoutes,
                    remote_addresses: statefile.lastDailyLog.remote_addresses
                }
                this.setState({
                    selectedDay: selectedDay
                });
                console.log(this.state.selectedDay);
                
            }
        })
    }

    watchArchivefile() {
        cockpit.file(this.state.ARCHIVEFILE_PATH).watch((content) => {
            if (content) {
                var archivefile = JSON.parse(content)
                this.setState({
                    dailyLogs: archivefile.logs
                })
                this.getSelectedDay(); //Get log dates to display date choice menu 
            } else {
                console.log("Error reading archivefile. Empty ?");
            }
        })
    }

    getSelectedDay() {
        let dates = []
        let months = []
        for (let i in this.state.dailyLogs) {
            var date = this.state.dailyLogs[i].date.split('-')[2]
            var month = this.state.dailyLogs[i].date.split('-')[1] + '-' + this.state.dailyLogs[i].date.split('-')[0]
            if (!(months.includes(month))) {
                months.push(month)
            }
            if (!dates.includes(date)) {
                dates.push(date)
            }
        }
        return [dates, months]
    }

    getReqStatusCountData() {
        return {
            datasets: [{
                data: [this.state.selectedDay.nbSuccessRequests, this.state.selectedDay.nbFailedRequests],
                backgroundColor: [
                    'rgba(45, 221, 44, 0.4)',
                    'rgba(221, 44, 44, 0.4)'
                ]
            }],
            labels: [
                'Success',
                'Failed'
            ]
        }
    }

    getVisitedRoutesData() {
        let routesDataDict = this.state.selectedDay.accessedRoutes
        let myData = []
        let myLabels = []

        if (!routesDataDict) {
            return EMPTY_GRAPH
        }

        for (var key in routesDataDict) {
            myLabels.push(key)
            myData.push(routesDataDict[key])
        }
        console.log(myLabels);
        console.log(myData);

        return {
            datasets: [{
                data: myData,
                backgroundColor: [
                    'rgba(45, 221, 44, 0.4)',
                    'rgba(221, 44, 44, 0.4)'
                ]
            }],
            labels: myLabels
        }
    }
    
    getVisitedApp() {
        console.log("ENTERING getVisitedApp");
        
        let appDataDict = this.state.selectedPeriod.accessedRoutesArchive
        if (!appDataDict) {
            return EMPTY_GRAPH
        }
        console.log(appDataDict);
        
        let myData = []
        let myLabels = []
        for (var key in appDataDict) {
            myLabels.push(key)
            myData.push(appDataDict[key])
        }
        console.log(myData);
        console.log(myLabels);
        
        console.log("ENDING getVisitedApp - appDataDict was initially defined");
        return {
            datasets: [{
                data: myData,
                backgroundColor: [
                    'rgba(45, 221, 44, 0.4)',
                    'rgba(221, 44, 44, 0.4)'
                ]
            }],
            labels: myLabels
        }

    }

    handleChange() {
        var startDay = this.refs.startDay.value
        var startMonth = this.refs.startMonth.value.split('-')[0]
        var startYear = this.refs.startMonth.value.split('-')[1]
        var endDay = this.refs.endDay.value
        var endMonth = this.refs.endMonth.value.split('-')[0]
        var endYear = this.refs.endMonth.value.split('-')[1]

        var startedDate = startYear + '-' + startMonth + '-' + startDay
        var endedDate = endYear + '-' + endMonth + '-' + endDay
        var startDate = new Date(startedDate)
        var endDate = new Date(endedDate)
        var elapsedStartDate = startDate.getTime()
        var elapsedEndDate = endDate.getTime()
        var nbSuccessRequests = 0
        var nbFailedRequests = 0
        var bar = {}
        var res = []

        var data = this.state.dailyLogs
        if (elapsedEndDate < elapsedStartDate) {
            console.log("Wrong dates")
        } else {
            let i = 0;
            let j = 0
            let counting = false;
            while (i < data.length) {
                if (data[i].date == startedDate) {
                    counting = true;
                }
                if (data[i].date == endedDate) {
                    counting = false
                }
                if (counting) {
                    res.push(data[i])
                }
                if (startedDate == endedDate && data[i].date == startedDate) {
                    res.push(data[[i]])
                }
                i++;
            }

            while (j < res.length) {
                nbSuccessRequests = nbSuccessRequests + res[j].nbSuccessRequests
                nbFailedRequests = nbFailedRequests + res[j].nbFailedRequests
                for (var key in res[j].accessedRoutes) {
                    if (!(key in bar)) {
                        bar[key] = res[j].accessedRoutes[key]
                    } else {
                        bar[key] = bar[key] + res[j].accessedRoutes[key]
                    }
                }

                j++;
            }
            console.log("BAR")
            console.log(bar)
            
            this.setState({
                selectedPeriod: {
                    nbSuccessRequests: nbSuccessRequests,
                    nbFailedRequests: nbFailedRequests,
                    accessedRoutes: bar
                }
            })
            console.log(this.state.dailyLogs[0])
            console.log(data)
            console.log(res)
            i = 0
        }
    }

    getReqArchiveCountData() {
        console.log(this.state.selectedPeriod.failedRequests)
        return {
            datasets: [{
                data: [this.state.selectedPeriod.successedRequests, this.state.selectedPeriod.failedRequests],
                backgroundColor: [
                    'rgba(45, 221, 44, 0.4)',
                    'rgba(221, 44, 44, 0.4)'
                ]
            }],
            labels: [
                'Success',
                'Failed'
            ]
        }
    }

    render() {
        const reqStatusCount = this.getReqStatusCountData();
        const visitedRoutesCount = this.getVisitedRoutesData();
        const visitedApp = this.getVisitedApp();
        const dates = this.getSelectedDay()[0];
        const months = this.getSelectedDay()[1];

        let optionItems = dates.map((date) =>
            <option value={date}>{date}</option>
        );
        let optionItemsMonth = months.map((month) =>
            <option value={month}>{month}</option>
        );

        return (

            <div className="container-fluid">

                <Tabs activeTab={{id: "live"}}>
                    
                    <Tabs.Tab id="live" title="Live">
                        <div style={{ padding: 10 }}>
                            <h2>Metrics configuration</h2>
                            <p>Watching file : {this.state.STATEFILE_PATH}</p>
                            <h3>Request status</h3>
                            <Doughnut
                                data={reqStatusCount}
                                width={100}
                                height={50}
                                options={{ maintainAspectRatio: false }}
                            />

                            <h3>Visited Routes</h3>
                            <Bar
                                data={visitedRoutesCount}
                                width={100}
                                height={50}
                                options={{
                                    scales: {
                                        yAxes: [{
                                            ticks: {
                                                beginAtZero: true
                                            }
                                        }]
                                    }
                                }}/>
                        </div>
                    </Tabs.Tab>

                    <Tabs.Tab id="archive" title="Archive">
                        <h3>App Visited </h3>
                        <div >
                            <label for='Start Day'> Start Day: </label>
                            <select name='Start Day' ref="startDay">
                                {optionItems}
                            </select>
                            <label for='Start Month' > Start Month: </label>
                            <select name='Start Month' ref="startMonth">
                                {optionItemsMonth}
                            </select>
                            <label for='btn'></label>
                            <label for='End Day'>End Day: </label>
                            <select name='End Day' ref="endDay">
                                {optionItems}
                            </select>
                            <label for='End Month'> End Month: </label>
                            <select name='End Month' ref="endMonth">
                                {optionItemsMonth}
                            </select>
                            <label for='btn'></label>
                            <button onClick={this.handleChange} name='btn'>Select</button>
                        </div>
                        <div style={{ padding: 10 }}>
                            <Doughnut
                                data={this.getReqArchiveCountData()}
                                width={100}
                                height={275}
                                options={{ maintainAspectRatio: false }}
                            />
                        </div>
                        <div>
                            <h3>App Visited </h3>

                            <Bar
                                data={visitedApp}
                                width={100}
                                height={50}
                                options={{
                                    scales: {
                                        yAxes: [{
                                            ticks: {
                                                beginAtZero: true
                                            }
                                        }]
                                    }
                                }}
                            />
                        </div>
                    </Tabs.Tab>
                </Tabs>


            </div>
        );
    }
}
